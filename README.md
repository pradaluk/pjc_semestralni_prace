# Zpráva k semestrální práci z předmětu Programování v C/C++

## Popis zadání
Na vstupu programu je list měst a druh potraviny dostupný v daném městě. Dále jsou na vstupu zadané dostupné cesty mezi městy. Program na vstupu dostane hodnotu minimálního požadovaného počtu různých potravin v každém městě a vypočte cenu potřebnou k dopravě potravin do všech měst tak, aby v každém městě bylo požadované množství různých potravin. Na výstupu také vypíše cenu potřebnou pro jednotlivá města a druhy potravin do něj dovezených.
## Popis implementace
Program ze standardního vstupu načte data o městech, jejich potravinách a cestách mezi nimi. Vytvoří 2d pole pro sousedy, kde první rozměr jsou indexy měst a druhý jsou jeho sousedé.
Hlavní algoritmus spočívá v postupném procházení měst a hledání potravin u jejich sousedů. Dokud jsou k dispozici města a nebo dokud není ve městě potřebné množství potravin, program bere města z fronty nenavštívených sousedů. U každého města zkontroluje zda je jeho potravina vhodná pro dovezení, tzn. že ještě nebyla dovezena do města. Pokud ano přičte se jeho vzdálenost k ceně potřebné pro konkrétní město a přidá se potravina do pole dodaných potravin. Pokud program nenašel dostatek potravin u přímých sousedů, pak pokračuje s městy ve frontě, kde jsou další sousedé o jednu cestu dále.

## Testovací případy

| Test | Vstup | Výstup |
| ---- | ----- | ------ |
| 1. | 5 5 4 3 <br> 0 1 3 2 1 <br> 0 1 <br> 2 1 <br> 2 3 <br> 3 0 <br> 4 3 | Celková cena potřebná pro zajištění pestrosti trhu je: 11 <br> Cena pro město 0 je: 2 a jeho potraviny jsou:  0 1 2 <br> Cena pro město 1 je: 2 a jeho potraviny jsou:  1 0 3 <br> Cena pro město 2 je: 2 a jeho potraviny jsou:  3 1 2 <br> Cena pro město 3 je: 2 a jeho potraviny jsou:  2 3 0 <br> Cena pro město 4 je: 3 a jeho potraviny jsou:  1 2 3 <br> |
| 2. | 3 2 1 1 <br> 0 0 0 <br> 0 1 <br> 1 2 <br> | Celková cena potřebná pro zajištění pestrosti trhu je: 0 <br> Cena pro město 0 je: 0 a jeho potraviny jsou:  0 <br> Cena pro město 1 je: 0 a jeho potraviny jsou:  0 <br> Cena pro město 2 je: 0 a jeho potraviny jsou:  0 <br> Cena pro město 3 je: 0 a jeho potraviny jsou:  0 <br> Cena pro město 4 je: 0 a jeho potraviny jsou:  0 <br> |
| 3. | 5 4 5 3 <br> 0 3 1 4 2 <br> 0 1 <br> 2 1 <br> 3 1 <br> 4 1 | Celková cena potřebná pro zajištění pestrosti trhu je: 14 <br> Cena pro město 0 je: 3 a jeho potraviny jsou:  0 3 1 <br> Cena pro město 1 je: 2 a jeho potraviny jsou:  3 0 1 <br> Cena pro město 2 je: 3 a jeho potraviny jsou:  1 3 0 <br> Cena pro město 3 je: 3 a jeho potraviny jsou:  4 3 0 <br> Cena pro město 4 je: 3 a jeho potraviny jsou:  2 3 0 |
