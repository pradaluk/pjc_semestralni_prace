#include <iostream>
#include <vector>
#include <queue>
#include <algorithm>
#include <thread>
#include "set"

using namespace std;

void print_usage(std::string const& exe_name) {
    std::clog <<
    "Formát vstupu:\n"
    "\n"
    "První jsou dvě čísla N a M; číslo N udávající počet měst v síti a číslo M udávající počet silnic v síti.\n"
    "Následují další dvě čísla P a Q; číslo P udávající počet různých druhů potravin a číslo Q udávající minimální počet druhů potravin potřebných pro zajištění pestrosti trhu.\n"
    "Města i druhy potravin jsou číslovány od nuly, mají tedy čísla 0, 1, ..., N - 1, resp. 0, 1, ..., P - 1.\n"
    "Dále je N čísel a0, a1, ..., an - 1, 0 ≤ a_i < P. i-té číslo ai udává druh potraviny, který je k dispozici v i-tém městě.\n"
    "Poté následuje M dvojic čísel, popisující silnice mezi městy. X a Y, 0 ≤ x, y < N, x ≠ y, udávající, že v síti vede silnice mezi městy x a y.";
}

bool is_help(std::string const& argument) {
    return argument == "--help" || argument == "-h";
}

int compute (){
    int city_number, path_number, food_type_number, needed,needed_on_city = 0;
    cout << "Zadejte počet měst:" << endl;
    cin >> city_number;
    if(city_number < 1){
        cerr << "Nesprávný vstup";
        return 1;
    }
    cout << "Zadejte počet cest mezi městy:" << endl;
    cin >> path_number ;
    if(path_number < 0){
        cerr << "Nesprávný vstup";
        return 1;
    }
    cout << "Zadejte počet různých druhů potravin:" << endl;
    cin >> food_type_number ;
    if(food_type_number < 0){
        cerr << "Nesprávný vstup";
        return 1;
    }
    cout << "Kolik různých druhů je potřeba v každém městě:" << endl;
    cin >> needed;
    if(needed < 0){
        cerr << "Nesprávný vstup";
        return 1;
    }
    vector<int> food;
    for(int g = 0; g < city_number; g++) {
        int value;
        cout << "Potravina je dostupná ve městě " << g << ": " << endl;
        cin >> value;
        food.push_back(value);
    }
    vector<vector<int> > sousede(city_number);
    vector<vector<int> > vypis(city_number);

    cout << "Zadejte cesty mezi městy ve formátu 'X Y', kde X a Y jsou čísla měst mezi kterými vede cesta. " << endl;
    for(int i = 0; i < path_number; i++){
        int from, to = 0;
        cout << i+1 << ". cesta:" << endl;
        cin >> from;
        cin >> to;
        if(from < 0 || from >= city_number || to < 0 || to >= city_number){
            cerr << "Nespravný vstup";
            return 1;
        }
        sousede[from].push_back(to);
        sousede[to].push_back(from);
    }
    int price_all = 0;
    //-------------------------------------------------------------------------------------
    for(int city_index = 0; city_index < city_number; city_index++) {
        needed_on_city = needed;
        set<int> visited;
        queue<pair<int,int> > fronta;
        int price = 0;
        auto p1 = make_pair(city_index,0);
        fronta.push(p1);
        vypis[city_index].push_back(food[city_index]);
        needed_on_city--;
        visited.insert(city_index);

        while(!fronta.empty() && needed_on_city > 0) {
            int mesto = fronta.front().first;
            int pozice = fronta.front().second;
            fronta.pop();
            for(unsigned int j = 0; j < sousede[mesto].size();j++) {
                auto it = visited.find(sousede[mesto][j]);
                if(it != visited.end())
                    continue;
                fronta.push(make_pair(sousede[mesto][j],pozice+1));
                bool notExist = false;
                for(unsigned int z = 0; z < vypis[city_index].size(); z++) {
                    if (vypis[city_index][z] == food[sousede[mesto][j]])
                        notExist = true;
                }
                if(!notExist){
                    needed_on_city--;
                    price += pozice + 1;
                    vypis[city_index].push_back(food[sousede[mesto][j]]);
                }
                if(needed_on_city <= 0)
                    break;
            }
        }
        vypis[city_index].push_back(price);
        price_all = price_all + price;
    }
    cout << "Celková cena potřebná pro zajištění pestrosti trhu je: ";
    cout << price_all << "\n";
    for(int i = 0; i < city_number; i++) {
        cout << "Cena pro město " << i << " je: " << vypis[i][needed] << " a jeho potraviny jsou: ";
        for(unsigned int j = 0; j < needed; j ++)
            cout << " " <<  vypis[i][j];
        cout << "\n";
    }
    return 0;
}

int main(int argc, char *argv[]) {
    //--help,-h
    if (std::any_of(argv, argv+argc, is_help)) {
        print_usage(argv[0]);
        return 0;
    }
    thread t1(compute);
    t1.join();
}